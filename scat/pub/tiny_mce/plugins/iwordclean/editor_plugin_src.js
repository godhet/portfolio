// Word Format Cleanup Plugin v 0.3
//
// Function  ported from 
//	HTMLArea v3.0 by interactivetools.com, inc. & dynarch.com
//		A slight variation of the word cleaner code posted
//		by Weeezl (user @ InteractiveTools forums).
//
// TinyMCE port by idle sign
// v 0.1 - 23/07/2008
// v 0.2 - 15/09/2008 - shifted cells bugfix
// v 0.3 - 06/10/2008 - word stylish comments killed, Mso class search bugfix

(function() {

	tinymce.PluginManager.requireLangPack('iwordclean');
	tinymce.create('tinymce.plugins.iwordcleanPlugin', {

		init : function(ed, url) {
			ed.addCommand('mceIWordClean', function() {
				var D = ed.getContent();
				if (D.indexOf('class=Mso') >= 0 || D.indexOf('class="Mso') >= 0 || D.search('style=\"[^\"]*mso-') >=0) {

					// make one line
					D = D.replace(/\r\n/g, ' ').
						replace(/\n/g, ' ').
						replace(/\r/g, ' ').
						replace(/\&nbsp\;/g,' ');

					// keep tags, strip attributes
					D = D.replace(/ class=[^\s|>]*/gi,'').
						replace(/ style=\"[^>]*\"/gi,'').
						replace(/ align=[^\s|>]*/gi,'');

					//clean up tags
					D = D.replace(/<b [^>]*>/gi,'<b>').
						replace(/<i [^>]*>/gi,'<i>').
						replace(/<li [^>]*>/gi,'<li>').
						replace(/<ul [^>]*>/gi,'<ul>');

					// replace outdated tags
					D = D.replace(/<b>/gi,'<strong>').
						replace(/<\/b>/gi,'</strong>');

					// kill unwanted tags
					D = D.replace(/<\?xml:[^>]*>/g, '').	// Word xml
						replace(/<\/?st1:[^>]*>/g,'').		// Word SmartTags
						replace(/<\/?[a-z]\:[^>]*>/g,'').	// All other funny Word non-HTML stuff
						replace(/<\/?font[^>]*>/gi,'').		// Disable if you want to keep font formatting
						replace(/<\/?span[^>]*>/gi,' ').
						replace(/<\/?div[^>]*>/gi,' ').
						replace(/<\/?pre[^>]*>/gi,' ').
						replace(/<\/?h[1-6][^>]*>/gi,' ').
						replace(/<\/?col[^>]*>/gi,'').
						replace(/<!-- [^>]* -->/gi,'');

					//remove empty tags
					D = D.replace(/<strong><\/strong>/gi,'').
						replace(/<i><\/i>/gi,'').
						replace(/<P[^>]*><\/P>/gi,'');

					// nuke double spaces
					D = D.replace(/  */gi,' ');

					// make tables
					D = D.replace(/<table[^>]*>/gi,'<table cellspacing="0" cellpadding="2" width="100%">');
					D = D.replace(/<td[^crav>]*((align|valign|colspan|rowspan)=[^ >]+)?[^avcr>]*((align|valign|colspan|rowspan)=[^ >]+)?[^avcr>]*((align|valign|colspan|rowspan)=[^ >]+)?[^avcr>]*((align|valign|colspan|rowspan)=[^ >]+)?[^>]*>/gi,'<td $1 $3 $5 $7>');
					D = D.replace(/<tr[^>]*>/gi,'<tr>');
				}
				ed.setContent(D);
			});

			ed.addButton('iwordclean', {
				title : 'iwordclean.desc',
				cmd : 'mceIWordClean',
				image : url + '/img/iwordclean.gif'
			});

			ed.onNodeChange.add(function(ed, cm, n) {
				cm.setActive('iwordclean', n.nodeName == 'IMG');
			});
		},

		createControl : function(n, cm) {
			return null;
		},

		getInfo : function() {
			return {
				longname : 'Word Format Remove (HTMLArea port)',
				author : 'Weeezl, Mihai Bazon, idle sign',
				authorurl : 'http://idlesign.ya.ru',
				infourl : 'http://dynarch.com',
				version : "0.3"
			};
		}
	});
	tinymce.PluginManager.add('iwordclean', tinymce.plugins.iwordcleanPlugin);
})();