// v1.2

// �� ����� � �������� �� min �� max minv ~ �� '-'
function isNotValidNumber(text, allow_empty, minv, maxv) {
  var msg = "���� \"%d\". �������� ������� �� ������� ["+minv+"; "+maxv+"]";
	var result = isNotSmth(text, 'INT', allow_empty);  if (result) { return result; }
  if ((!allow_empty) && (text > maxv || text < minv)) { return msg; }
  return false;
}

// �� ������� (! '' ��� '   ')
function isEmpty(text) {
  var msg = "���� \"%d\". ������ ���� ���������";
  text = "" + text;
  for (var i=0; i < text.length; i++) {
    if (text.substring(i, i+1) != " ") {return false} 
  }
  return msg;
}

// �������� �� E-mail (@ � ������ � . ����� @ � ����� 2 �������� ����� .)
function isNotEmail(text) {
	var msg = "���� \"%d\". ������ ����� ������ ������ ��. ����� (user@domain.ru)";
  if(isEmpty(text)) return isEmpty(text);
  var pos_amp= text.indexOf("@");
  var pos_point= text.lastIndexOf(".");
  if (pos_amp == -1) return msg; 
  if (pos_point < pos_amp) return msg;
  if (2 > (text.length - pos_point)) return msg; 
  return false;
}

// �������� �� ������� �����-���� �������� ('-' � REAL ��������� ������ � ������� ������)
function isNotSmth(text, type, allow_empty, mincount, maxcount) {
  var posymb, msg;
  if (type == 'LATIN') {posymb = "_0123456789qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM"; msg = "���� \"%d\". ������ ��������� ������ ��������� ������� � �����";}
  if (type == 'PHONE') {posymb = "+-()0123456789 "; msg = "���� \"%d\". ����� ��������� ������ ����� � ������� +-()";}
  if (type == 'INT')   {posymb = "0123456789"; msg = "���� \"%d\". ������ ��������� ������ �����";}
  if (type == 'DBDATE')   {posymb = "0123456789."; msg = "���� \"%d\". ������ ���� � ������� ���� ��.��.����";}
  if (type == 'REAL')  {posymb = ".0123456789"; msg = "���� \"%d\". ������ ��������� ������ ����� � ����� \".\"";}
	if (type == 'ALMOST_ALL') {posymb = " ~,.+-=@#$^&*()_0123456789qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM������������������������������������������������������������������"; msg = "���� \"%d\". �� ������ ��������� ������������������ ��������";}
  text = "" + text;
  if(!allow_empty && isEmpty(text)) return isEmpty(text);
	if ((mincount || maxcount) && isNotCount(text, mincount, maxcount)) return isNotCount(text, mincount, maxcount);
  for (var i=0; i< text.length; i++){
     TempChar=text.substring(i, i+1);
     if (posymb.indexOf (TempChar, 0) == -1) return msg;
  }
  return false;
}

function isNotCount(text, mincount, maxcount) {
  var msg = (mincount == maxcount ? "���� \"%d\". ���������� ������ ������ ���� ������ " + mincount : "���� \"%d\". ���������� ������ ������ ���� �� ����� " + mincount + " � �� ����� " + maxcount);
	if (text.length < mincount || text.length > maxcount) {return msg}
	return false;
}

// ���� �� �����
function isNotEq(text, text_check, latin) {
  var msg = "���� \"%d\". ������ ���������";
  if (latin) {if (isNotSmth(text, 'LATIN', 0)) return (isNotSmth(text, 'LATIN', 0)); if (isNotSmth(text_check, 'LATIN', 0)) return (isNotSmth(text_check, 'LATIN', 0)); }
  if (text != text_check) {return msg}
  return false;
}

// SELECT ���� ��������� ������� = '' ��� ���� = '#' � �� ��������� _other
function isNotSelect(fobj, nameid) { // form object & name select
  var msg_choiced = "���� \"%d\". �� ������� �������� �� ������ ������";
  var msg_other = "���� \"%d\". ��� �������� ����� \"������\", ������� ��������� ��� �������� � ���� ����� ����";
  var msg_dir = "���� \"%d\". ������ ������ �� ������������ ��� ������";
  var selobj = fobj.elements[nameid];
  var otherobj = (fobj.elements[selobj.name+"_other"] ? fobj.elements[selobj.name+"_other"] : null);
  for (var j=0; j<selobj.length; j++) { // ��� �� ����� selecta
    if (selobj.options[j].selected) {	// ����� select �������
	  if (selobj.options[j].value == "") {return msg_choiced}
		if (selobj.options[j].value == "" && otherobj && isEmpty(otherobj.value)) {return msg_choiced}
	  else if (selobj.options[j].value == '#' && otherobj && isEmpty(otherobj.value)) {return msg_other} // # � ���������� ������� _other � = ""
	  else if (selobj.options[j].value.indexOf("!") > -1) {return msg_dir} } }
  return false;
}

// RADIO CB ���� ��������� ������� = '' ��� ���� = '#' � �� ��������� _other. v1.01
function 	(fobj, nameid, fromel) {
  var msg_choiced = "���� \"%d\". �� ������� �������� �� �������������";
  var msg_other = "���� \"%d\". ��� �������� ����� \"������\", ������� ��������� ��� �������� � ���� ����� ����";
  var cr_have_checked = false;
  for(var i=fromel; i<fobj.length; i++) { // ��� �� �������� ����� (1 radio = 1 ������)
		if ((fobj.elements[i].type == "radio" || fobj.elements[i].type == "checkbox") && fobj.elements[i].name.indexOf(nameid) == 0 && fobj.elements[i].checked) { // ��������� radio, name, checked
	  cr_have_checked = true;
	  if (fobj.elements[i].value == '#' && fobj.elements[fobj.elements[i].name+"_other"] && fobj.elements[fobj.elements[i].name+"_other"].value == "") {return msg_other} } }// #, _other, .value=�����
  if (!cr_have_checked) {return msg_choiced}
  return false;
}
