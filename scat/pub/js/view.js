// cb_ary(this.form.del_ids)
function cb_ary(cbs) {
  if (!cbs) {return} 
	if (!cbs.length) {cbs.checked=(cbs.checked ? 0 : 1); return} // 1 element
	var status=(cbs[0].checked ? 0 : 1);
	for(var i=0; i<cbs.length; i++) {cbs[i].checked=status;} 
}

// cb_lst(this.form, 'ehide', '1,25')
function cb_lst(fm, name, idstr) {
  var ids=idstr.split(','); 
	if (!ids[0] || !fm.elements[name+'_'+ids[0]]) {return} 
	var status=(fm.elements[name+'_'+ids[0]].checked ? 0 : 1); 
	for(var i=0; i<ids.length; i++) {fm.elements[name+'_'+ids[i]].checked=status;} 
}
