1; # Bookmarks: 0,0 0,0 0,1

if( $fid eq '' ){
  my $sth=$dbh->prepare("SELECT id,citata$lang as citata,date_format(edate,'%d/%m/%x') as edate FROM news WHERE ehide is null ORDER BY eorder desc,id desc  LIMIT 3");
  $sth->execute();
  #$HTML_SITE{m_title}="SELECT id,citata$lang as citata,date_format(edate,'%d.%m.%x') as edate FROM news ORDER BY eorder,id";
  my @news;
  while( my $qwe=$sth->fetchrow_hashref() ){
    push @news, { link=>"news-$qwe->{id}.html", citata=>$qwe->{citata}, date=>$qwe->{edate} }
  }               
  $HTML_SITE{news}=\@news;
  
  my $sph=$dbh->prepare("SELECT id,citata$lang as citata,img_ind,url FROM specials ORDER BY eorder desc,id desc");
  $sph->execute();
  my (@spec); 
  while( my $qwe=$sph->fetchrow_hashref() ){
    $qwe->{img_ind}='/images/scat.jpg' if( $qwe->{img_ind} eq ''  );
    push @spec,{ ru=>$HTML_SITE{ru}, en=>$HTML_SITE{en}, kz=>$HTML_SITE{kz}, spec_img=>$qwe->{img_ind}, spec_descr=>$qwe->{citata}, spec_link=>"deals-$qwe->{id}.html" };
  }  
  $HTML_SITE{spec}=\@spec;
  
  my $cph=$dbh->prepare("SELECT id,name$lang as name,citata$lang as citata,img_thumb,url FROM country_r ORDER BY eorder,id");
  $cph->execute();
  my (@country); 
  while( my $qwe=$cph->fetchrow_hashref() ){
    push @country,{ country_img=>$qwe->{img_thumb},name=>$qwe->{name}, link=>"$qwe->{url}.html" };
  }  
  $HTML_SITE{country}=\@country;
  
#форма поиска
  
  #require "module.pl";
  #my $city_str=&response('http://booking.scat.kz/is2009/export-city-xml?type=routeMap');
  my @cities;
  push @cities, { codeEN=>"SCO", codeRUS=>"ААУ", id=>"ААУ", nameEN=>"Aktau",      nameRUS=>"Актау",   nameKZ=>"Ақтау" };
  push @cities, { codeEN=>"AKX", codeRUS=>"АКХ", id=>"АКХ", nameEN=>"Aktyubinsk", nameRUS=>"Актобе",  nameKZ=>"Ақтөбе" };
  push @cities, { codeEN=>"ALA", codeRUS=>"АЛА", id=>"АЛА", nameEN=>"Almaty",     nameRUS=>"Алматы",  nameKZ=>"Алматы" };
  push @cities, { codeEN=>"TSE", codeRUS=>"АКЛ", id=>"АКЛ", nameEN=>"Astana",     nameRUS=>"Астана",  nameKZ=>"Астана" };
  push @cities, { codeEN=>"ASF", codeRUS=>"АСР", id=>"АСР", nameEN=>"Astrakhan",  nameRUS=>"Астрахань", nameKZ=>"Астрахан" };
  push @cities, { codeEN=>"GUW", codeRUS=>"АТЫ", id=>"АТЫ", nameEN=>"Atyrau",     nameRUS=>"Атырау",  nameKZ=>"Атырау" };
  push @cities, { codeEN=>"BAK", codeRUS=>"БАК", id=>"БАК", nameEN=>"Baku",       nameRUS=>"Баку",    nameKZ=>"Баку" };
  push @cities, { codeEN=>"BKK", codeRUS=>"БКК", id=>"БКК", nameEN=>"Bangkok",    nameRUS=>"Бангкок", nameKZ=>"Бангкок" };
  push @cities, { codeEN=>"VOG", codeRUS=>"ВГГ", id=>"ВГГ", nameEN=>"Volgograd",  nameRUS=>"Волгоград", nameKZ=>"Волгоград" };
  push @cities, { codeEN=>"DZN", codeRUS=>"ДЗН", id=>"ДЗН", nameEN=>"Zhezkazgan", nameRUS=>"Джезказган", nameKZ=>"Жезқазған" };
  push @cities, { codeEN=>"EVN", codeRUS=>"ЕВН", id=>"ЕВН", nameEN=>"Yerevan",    nameRUS=>"Ереван",  nameKZ=>"Ереван" };
  push @cities, { codeEN=>"SZI", codeRUS=>"ЗСН", id=>"ЗСН", nameEN=>"Zaisan",     nameRUS=>"Зайсан",  nameKZ=>"Зайсаң" };
  push @cities, { codeEN=>"KGF", codeRUS=>"КГД", id=>"КГД", nameEN=>"Karaganda",  nameRUS=>"Караганда", nameKZ=>"Қарағанды" };
  #push @cities, { codeEN=>"IEV", codeRUS=>"ИЕВ", id=>"ИЕВ", nameEN=>"Kiev",       nameRUS=>"Киев",      nameKZ=>"Киев" };
  push @cities, { codeEN=>"KOV", codeRUS=>"КЧТ", id=>"КЧТ", nameEN=>"Kokshetau",  nameRUS=>"Кокчетав",  nameKZ=>"Көкшета́у" };
  push @cities, { codeEN=>"KSN", codeRUS=>"КТН", id=>"КТН", nameEN=>"Kostanay",   nameRUS=>"Костанай",  nameKZ=>"Қостанай" };
  push @cities, { codeEN=>"KRR", codeRUS=>"КРР", id=>"КРР", nameEN=>"Krasnodar",  nameRUS=>"Краснодар", nameKZ=>"Краснодар" };
  push @cities, { codeEN=>"KZO", codeRUS=>"КЗО", id=>"КЗО", nameEN=>"Kzyl-Orda",       nameRUS=>"Кызылорда", nameKZ=>"Қызылорда" };
  push @cities, { codeEN=>"MCX", codeRUS=>"МХЛ", id=>"МХЛ", nameEN=>"Makhachkala",     nameRUS=>"Махачкала", nameKZ=>"Махачкала" };
  push @cities, { codeEN=>"MRV", codeRUS=>"МРВ", id=>"МРВ", nameEN=>"Mineralnye Vody", nameRUS=>"Минеральные Воды", nameKZ=>"Минеральные Воды" };
  push @cities, { codeEN=>"MOW", codeRUS=>"МОВ", id=>"МОВ", nameEN=>"Moscow",          nameRUS=>"Москва",    nameKZ=>"Мәскеу" };
  push @cities, { codeEN=>"OVB", codeRUS=>"ОВБ", id=>"ОВБ", nameEN=>"Novosibirsk",     nameRUS=>"Новосибирск",    nameKZ=>"Новосибирск" };
  push @cities, { codeEN=>"ROV", codeRUS=>"РОВ", id=>"РОВ", nameEN=>"Rostov-on-Don",     nameRUS=>"Ростов-на-Дону",    nameKZ=>"Ростов-на-Дону" };     
  push @cities, { codeEN=>"PPK", codeRUS=>"ПРА", id=>"ПРА", nameEN=>"Petropavlovsk",   nameRUS=>"Петропавловск",  nameKZ=>"Петропавл" };
  push @cities, { codeEN=>"PLX", codeRUS=>"СПЛ", id=>"СПЛ", nameEN=>"Semipalatinsk",   nameRUS=>"Семипалатинск",  nameKZ=>"Семей" };
  push @cities, { codeEN=>"AER", codeRUS=>"СОЧ", id=>"СОЧ", nameEN=>"Sochi",           nameRUS=>"Сочи",      nameKZ=>"Сочи" };
  push @cities, { codeEN=>"IST", codeRUS=>"ИСТ", id=>"ИСТ", nameEN=>"Istanbul",        nameRUS=>"Стамбул",   nameKZ=>"Ыстамбұл" };
  push @cities, { codeEN=>"DMB", codeRUS=>"ДМБ", id=>"ДМБ", nameEN=>"Zhambyl",         nameRUS=>"Тараз",     nameKZ=>"Тараз" };
  push @cities, { codeEN=>"TBS", codeRUS=>"ТБС", id=>"ТБС", nameEN=>"Tbilisi",         nameRUS=>"Тбилиси",   nameKZ=>"Тбилиси" };
  push @cities, { codeEN=>"URA", codeRUS=>"УРЛ", id=>"УРЛ", nameEN=>"Uralsk",          nameRUS=>"Уральск",   nameKZ=>"Орал" };
  push @cities, { codeEN=>"UZR", codeRUS=>"УРД", id=>"УРД", nameEN=>"Urdzhar",         nameRUS=>"Урджар",    nameKZ=>"Урджар" };
  push @cities, { codeEN=>"UKK", codeRUS=>"УКГ", id=>"УКГ", nameEN=>"Ust-Kamenogorsk", nameRUS=>"Усть Каменогорск", nameKZ=>"Өскемен" };
  push @cities, { codeEN=>"CIT", codeRUS=>"ШМТ", id=>"ШМТ", nameEN=>"Shymkent",        nameRUS=>"Шымкент",   nameKZ=>"Шымкент" };
  my @city;
  
  foreach ( @cities ){
    my ($name, $label);
    if( $lang eq '' || $lang eq '_kz' ){ $name= $_->{nameRUS}; $label=$_->{nameRUS}; }
    if( $lang eq '_kz' ){ $label=$_->{nameKZ}; } 
    if( $lang eq '_en' ){ $name=$_->{nameEN}; $label=$_->{nameEN}; }
    push @city, { name=>$name, label=>$label }
  }  
  $HTML_SITE{city}=\@city;
#-------------
  
  my $pth=$dbh->prepare("SELECT img,name$lang as name,descr$lang as descr,descr2$lang as descr2,url FROM slideshow WHERE ehide is null ORDER BY eorder,id");
  $pth->execute();
  my (@slide,@name,@descr,@descr2,@url);
  while( my $qwe=$pth->fetchrow_hashref() ){
    #$qwe->{name} =~ s/ /&nbsp;/g;
    #$qwe->{descr} =~ s/ /&nbsp;/g;
    #$qwe->{descr2} =~ s/ /&nbsp;/g;
    push @slide, "'$qwe->{img}'";
    push @name, "'$qwe->{name}'";
    push @descr, "'$qwe->{descr}'";
    push @descr2, "'$qwe->{descr2}'";
    push @url, "'$qwe->{url}'";
  }  
  $HTML_SITE{slide}=join ",", @slide;
  $HTML_SITE{name}=join ",", @name;
  $HTML_SITE{descr}=join ",", @descr;
  $HTML_SITE{descr2}=join ",", @descr2;
  $HTML_SITE{url}=join ",", @url;


}else{      
  my ($db) = $dbh->selectrow_hashref("SELECT id,name$lang as name,text$lang as text,img_inside,m_title$lang as m_title,m_description$lang as m_description,m_keywords$lang as m_keywords FROM slideshow WHERE id='$fid'");
  
  my $qwe=$dbh->prepare("SELECT id,name$lang as name,url FROM slideshow WHERE ehide is null ORDER BY eorder,id");
  $qwe->execute();
  while( my $qqq=$qwe->fetchrow_hashref() ){
    $link=( $qqq->{url} eq '' )?"doc-$qwe->{id}.html":"$qqq->{url}.html";
    my $active=( $act eq $qqq->{url} )?" class=\"active\"":"";
    push @sub_menu, { url=>"<a href=\"$link\"$active>$qqq->{name}</a>$ul" };      
  }
  $HTML_SITE{sub_menu}=\@sub_menu;
  $HTML_SITE{sub_title}=$dbh->selectrow_array("SELECT name$lang as name FROM slideshow WHERE id='$fid' LIMIT 1");
  
  $HTML_SITE{link_name}=$db->{name};
  $HTML_SITE{link_text}=$db->{text} if($db->{text} ne '');
  #$HTML_SITE{img}=$db->{img} if( $db->{img} ne '' );
  $HTML_SITE{img}=$db->{img_inside} if( $db->{img_inside} ne '' );
  $HTML_SITE{m_title}="$db->{name} | $param_name_site" if( $eng eq '' );

}  
1;