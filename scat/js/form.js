/*    
  author Andrew Osipov
  http://godhet.ru
  http://simplephotoweb.ru
*/

(function($){
  $.fn._form = function($){
    var formContainer=this;
    var options={};
    jQuery.extend( options,{
      fields: [],
      formName: '',
      autoLabelsGlobal: false,
      className: 'dynamic-form',
      action: '',
      method: 'post',
      verify: true,
      isRequired: false,
      requiredText: 'Поля, отмеченные * обязательны для заполнения',
      alertText: 'Заполните обязательные поля',
      successText: 'Ваше сообщение отправлено. После проверки модератором оно появится на нашем сайте. Спасибо за внимание!', 
      submitText:'Отправить',
      submit: function(){        
        if( form_verify() ){
          alert(options.successText)
          //form_empty();
          return true;
        }else alert( options.alertText );
        return false;
      }
    }, $);
    
    buildForm();
    formContainer.find('form').submit( options.submit );
    
    //--------------------------------------    
    function form_empty(){
      jQuery.each(options.fields, function(index, value){
        switch (value.type){
          case 'text':     value.element.find('input[type=text]').val( value.label ); break;            
          case 'textarea': value.element.find('textarea').val( value.label ); break;            
          default:         value.element.find('input[type=text]').val( value.label );
        }
      })
    }
        
    function form_verify(){  
      if( !options.verify ) return true;
      var result=true;
      jQuery.each(options.fields, function(index, value){
        if( value.required ){
          var element;
          switch (value.type){
            case 'text':     element=value.element.find('input[type=text]'); break;
            case 'textarea': element=value.element.find('textarea'); break;
            case 'select':   element=value.element.find('select'); break;
            default:         element=value.element.find('input[type=text]');
          }
          if( value.autoField && (element.val()==value.label || element.val()=='') ){
             element.addClass('field-error');
             result=false;
          }else if( !value.autoField && element.val()=='' ){
             element.addClass('field-error');
             result=false;
          }else{ element.removeClass('field-error'); }
        }
      })
      return result;
    }
    
    //строит форму
    function buildForm(){ 
      var html;
      html = "<form name='"+options.formName+"' class='"+options.className+"' action='"+options.action+"' method='"+options.method+"'></form>";
      formContainer.html(html);
      jQuery.each(options.fields, function(index, value){
         value.element=jQuery( input(value) );
         formContainer.find("form").append( value.element );
         if( value.required ) options.isRequired=true;
      })
      if( options.isRequired ) formContainer.find("form").before("<div class='required-text'>"+options.requiredText+"</div>")
      formContainer.find("form").append("<input type='submit' value='"+options.submitText+"' class=\"form-submit\" />");
      //formContainer.html(html);       
    }
    
    //генерирует поля input, textarea, submit
    function input(field){
      var obj=''; field.label+=(field.required)?"*":"";
      switch (field.type){
        case 'text':{ 
          if( field.autoField ){ obj="<div class='form-field'><input type='text' name='"+field.name+"' onFocus=\"javascript:if(this.value=='"+field.label+"')this.value=''\" onBlur=\"javascript:if(this.value=='')this.value='"+field.label+"'\" value=\""+field.label+"\" /></div>"; }
          else{ obj="<div class='form-field'><label for=''>"+field.label+"</label>"; obj+="<input type='text' name='"+field.name+"' /></div>"; }
        } break;  
        case 'hidden':{ 
          obj="<input type='hidden' name='"+field.name+"' value=\""+field.label+"\" />";          
        } break;      
        case 'textarea':{ 
          if( field.autoField ){ obj="<div class='form-field'><textarea name='"+field.name+"' onFocus=\"javascript:if(this.innerText=='"+field.label+"')this.innerText=''; if(this.value=='"+field.label+"') this.value=''\" onBlur=\"javascript:if(this.innerText=='')this.innerText='"+field.label+"'; if(this.value=='')this.value='"+field.label+"';\">"+field.label+"</textarea></div>"; }
          else{ obj="<div class='form-field'><label for=''>"+field.label+"</label>"; obj+="<textarea name='"+field.name+"'></textarea></div>"; }
        } break; 
        case 'select':{ 
          if( field.autoField ){ obj="<div class='form-field'><input type='text' name='"+field.name+"' onFocus=\"javascript:if(this.value=='"+field.label+"')this.value=''\" onBlur=\"javascript:if(this.value=='')this.value='"+field.label+"'\" value=\""+field.label+"\" /></div>"; }
          else{ 
            obj="<div class='form-field'><label for=''>"+field.label+"</label>"; 
            obj+="<select name='"+field.name+"'>";
            obj+="<option selected value=''>Выберите тренинг</option>"
            for( var key in field.value ){
              obj+="<option value='"+field.value[key].value+"'>"+field.value[key].name+"</option>"
            }
            obj+="</select></div>"; 
          }
        } break;         
        default:{ 
          if( field.autoField ){ obj="<div class='form-field'><input type='text' name='"+field.name+"' onFocus=\"javascript:if(this.value=='"+field.label+"')this.value=''\" onBlur=\"javascript:if(this.value=='')this.value='"+field.label+"'\" value=\""+field.label+"\" /></div>"; }
          else{ obj="<div class='form-field'><label for=''>"+field.label+"</label>"; obj+="<input type='text' name='"+field.name+"' /></div>"; }
        }
     }
     return obj;
    }
      
  }
})(jQuery)