/*    
  author Andrew Osipov
  http://godhet.ru
  http://simplephotoweb.ru
*/

(function($){
  $.fn.rotator = function($){
    this.empty().append('<div class="image-container"></div><div class="errors-container"></div>')
    var jqRotator=this, imageContainer=this.find('.image-container'), errorContainer=this.find('.errors-container'),slideInfo=jQuery('#slide-info'),bullet=slideInfo.find('ul')
    var images=[], links=[]
    var current=0,complete=false,began=null,imagesCount=0, moveOn=true  
    var options={}
    var defaults={
      width:900,
      height:600,
      details:'',
      autosize:true,
      stretch:true,
      speed: 1000,
      interval:7000,
      displayError:false
    } 
    
    if(-[1,]){
      var back=new Image()
      back.src='images/ajax-loader.gif'
      back.onload =function(){ start() }
      back.onerror=function(){ start() }
    }else{ start(); } 
     
     
    function start(){  
      if( setOptions() ){
        
        for(var i=0;i<$.images.length;i++){
          images[i]=new Array(2)
          images[i][0]=new Image()         
          images[i][0].src=$.images[i]
          images[i][1]=false  //image loaded
          images[i][2]=false  //image prepared
          images[i][3]=$.name[i]
          images[i][4]=$.descr[i]
          images[i][5]=$.descr2[i]
          images[i][6]=$.url[i]  
          jQuery(images[i][0]).attr('rel',i)
          var d=jQuery('<li><a href="#" rel="'+i+'"></a></li>')
          bullet.append(d)
          images[i][0].onload=function(){            
            var i=jQuery(this).attr('rel')
            imagesCount++
            complete=(imagesCount==$.images.length)?true:false
            images[i][1]=true
            jQuery(this).css({opacity:0, position:'absolute', left:0, top:0})
            if(began==null){
              began=true
              current=parseInt(i)
              firstShow()                            
            }        
          }  
          images[i][0].onerror=function(){
            showError('Изображение '+this.src+' не загружено')          
          }  
          bullet.find('a').click(function(){
            if(moveOn){
              moveOn=false
              current=parseInt( jQuery(this).attr('rel') )
              bulletChange()              
              imageContainer.children().before( images[current][0] )
              prepareImage(images[current][0])
              imageContainer.children().first().animate({'opacity':1.0},options.speed)
              imageContainer.children().last().animate({'opacity':0},options.speed,function(){ jQuery(this).remove(); moveOn=true; })
              bulletChange();
                                  
            }
            return false
          })                                                    
        }
      }else{
        showError('Критическая ошибка: один или несколько параметров заданы неверно!','fatal')
      } 
    }
    
    function setOptions(){
      options.autosize=($.autosize!=null)?$.autosize:defaults.autosize
      options.stretch=($.stretch!=null)?$.stretch:defaults.stretch
      if(options.autosize){ 
        options.width=jqRotator.width()
        options.height=jqRotator.height()
      }else{
        options.width=($.width!=null)?$.width:defaults.width
        options.height=($.height!=null)?$.height:defaults.height
      }   
      options.details=($.details!=null)?$.details:defaults.details;
      imageContainer.css({width:options.width,height:options.height,position:'absolute'})
      errorContainer.css({width:options.width,height:options.height,position:'absolute',overflowY:'auto',cursor:'pointer'})
      options.speed=($.speed!=null)?$.speed:defaults.speed
      options.interval=($.interval!=null)?$.interval:defaults.interval
      options.displayError=($.displayError!=null)?$.displayError:defaults.displayError
      options.imagesCount=0
      complete=false
      jqRotator.css({'background-image':'url(images/ajax-loader.gif)',backgroundPosition:'50% 50%',backgroundRepeat:'no-repeat',position:'relative'})
      
      /*errorContainer.click(function(){
        var i=current
        if( images[i][3]!='' || images[i][3]!=null )location.href=images[i][3]
      })*/
      
      errorContainer.hover(
        function(){
          var i=current
          if( images[i][3]!='' ) window.status=location.host+'/'+images[i][3]
        },
        function(){
          window.status=''
        }
      )   
      
      if($.images!=null ) return true
      else return false
    }
    
    function prepareImage(img){ //подготавливает изображение к показу
      if( !images[ jQuery(img).attr('rel') ][2] ){ //не готовь готовое и не руби срубленное
        var height=img.offsetHeight, width=img.offsetWidth
        if(options.autosize){
          var new_width=width, new_height=height, diff_width=width-jQuery(img).width(), diff_height=height-jQuery(img).height()
          if( width>options.width ){  //подгоняем размер фото по формуле пропорций
            new_width=options.width   //сж
            new_height=height*new_width/width
            if( new_height>options.height ){              
              new_width=new_width*options.height/new_height
              new_height=options.height
            }                 
          }else{
            if( height>options.height ){ //подгоняем размер фото по формуле пропорций
              new_height=options.height
              new_width=new_height*width/height
              if( new_width>options.width ){                
                new_height=options.width*new_height/new_width
                new_width=options.width
              }
            }else{
              if( height<options.height && options.stretch ){ //подгоняем размер фото по формуле пропорций
                new_height=options.height
                new_width=new_height*width/height
                if( new_width>options.width ){                
                  new_height=options.width*new_height/new_width
                  new_width=options.width
                }
              }else{
                if( width<options.width && options.stretch){  //подгоняем размер фото по формуле пропорций
                  new_width=options.width   //сж
                  new_height=height*new_width/width
                  if( new_height>options.height ){              
                    new_width=new_width*options.height/new_height
                    new_height=options.height
                  }                 
                }              
              }
            }
          }
          jQuery(img).css({height:Math.round(new_height-diff_height),width:Math.round(new_width-diff_width),marginLeft:(options.width-new_width)/2, marginTop:(options.height-new_height)/2})         
        }else 
          jQuery(img).css({marginLeft:(Math.abs(options.width-width))/2, marginTop:(Math.abs(options.height-height))/2})
          images[ jQuery(img).attr('rel') ][2]=true  //ставим метку Подготовлено           
      }
    }
    
    function firstShow(){  //запускает первое фото                 
      jqRotator.css({backgroundImage:'none'})
      imageContainer.append( images[current][0] )
      prepareImage(images[current][0])  
      imageContainer.children().animate({'opacity':1},options.speed)
      
      bulletChange()
      rotator()      
    }
    
    function rotator(index){   //сменяет фото       
      var timer=setInterval(function(){
        current=getCurrent()
        if(current>=0 && moveOn){   //нужно ли менять фото       
          moveOn=false
          imageContainer.children().before( images[current][0] )
          prepareImage(images[current][0])
          imageContainer.children().first().animate({'opacity':1.0},options.speed)
          imageContainer.children().last().animate({'opacity':0},options.speed,function(){ jQuery(this).remove(); moveOn=true; })
          bulletChange();          
        }
      },options.interval)
    }
    
    function bulletChange(){
      bullet.find('a.current').removeClass('current')
      bullet.find('a[rel='+current+']').addClass('current');
      slideInfo.find('#slide-info-title').html( images[current][3] );
      slideInfo.find('#slide-info-descr span').html( images[current][4] );
      slideInfo.find('#slide-info-descr2 span').html( images[current][5] );
      slideInfo.find('#slide-info-more span').html( '<a href="'+images[current][6]+'.html">'+options.details+' >></a>' );
    }
    
    function getCurrent(type){
      if(imagesCount>1){
        if(complete) return (current<images.length-1)?current+1:0
        else{
          for(var i=current+1;i<images.length;i++)
            if(images[i][1]) return i
          for(var i=0;i<images.length;i++)
            if(images[i][1]) return i          
        }
      }else return -1
    }
    
    function showError(message,type){
      if(options.displayError){       
        var e=document.createElement('p')
        if(type=='fatal') jQuery(e).css({background:'#f00',color:'#fff',padding:'0 0 0 5px'})
        else jQuery(e).css({background:'#333333',color:'#fff',padding:'0 0 0 5px',margin:'3px 0 3px 0'})
        jQuery(e).text(message).css({overflow:'auto',backgroundImage:'none'})
        errorContainer.append(e)
      }
    }   
  }
})(jQuery);