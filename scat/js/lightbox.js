(function($){
  $.fn._lightBox = function($){ 
    var boxContainer, fadeBack, closeX, contentBox, controls, submit, boxTitle, boxError, boxMessage, boxDescr, boxClose;
    var options={};
    jQuery.extend( options,{
      boxWidth: 350,
      boxHeight: 400,
      boxPadding:'5px 5px 5px 5px',
      controlsHeight: 15,
      submitText: 'Отправить',
      fadeClickClose: true,
      controlsShow: !true,
      form: 'order', //order, contacts
      ajaxLoader: 'images/ajax-loader.gif'       
    },$)
        
    var img=new Image;
      img.src=options.ajaxLoader; 
    _build();
    _prepare(); 
    
    var image=new Image();
    image.src=options.imgUrl
    jQuery(image).load(function(){
      jQuery(image).css({ width:options.boxWidth, height:options.boxHeight, opacity:0 })
      contentBox.css({ background: 'none' }).append(image).children().animate({ opacity:1 })
      boxDescr.css({ width:(jQuery(window).width()-boxContainer.width()-20)/2 }).html(options.imgDescr)
      boxClose.css({ width:(jQuery(window).width()-boxContainer.width()-10)/2 })
    })   
       
    function _remove(){
      fadeBack.unbind('click').fadeOut().remove();
      closeX.unbind('click');
      boxContainer.fadeOut().remove();
      boxDescr.fadeOut().remove();
      boxClose.fadeOut().remove();
    }
    
    function _prepare(){
      fadeBack
        .css({ width:'100%', height:'100%', position:'fixed', top:0, left:0, background:'#000', opacity:0.8, zIndex:20000 })
        .click(function(){ if( options.fadeClickClose ) _remove(); });
      closeX.hide()
        .css({ position:'absolute', color:'#000', top:1, right:3, cursor:'pointer' })
        .click(function(){ _remove(); });     
      var h=jQuery(window).height()-80;
      var w=options.width*h/options.height;
      options.boxWidth=w; options.boxHeight=h;
      boxContainer                          
        .css({ width:options.boxWidth, height:options.boxHeight, padding:options.boxPadding, position:'fixed', zIndex:20001, top:(jQuery(window).height()-options.boxHeight)/3 , left:(jQuery(window).width()-options.boxWidth)/2, background:'#fff', '-webkit-box-shadow': '1px 1px 5px #2c2c2c', '-moz-box-shadow': '1px 1px 5px #2c2c2c', 'box-shadow': '1px 1px 5px #2c2c2c' });
      contentBox
        .css({ width:boxContainer.width(), height:boxContainer.height(), background:'url('+options.ajaxLoader+') 50% 50% no-repeat' });
      boxTitle
        .css({ width:boxContainer.width(), height:10, position:'absolute', top:2, fontWeight:'bold' });
      boxError
        .css({ height:10, position:'absolute', bottom:15, color:'#f00', textShadow:'0px 0px 1px #ff0707' });
      boxMessage
        .css({ height:10, position:'absolute', bottom:8, color:'#4ba113', textShadow:'0px 0px 1px #52b014' });
      boxDescr
        .css({ height:jQuery(window).height()-20, position:'fixed', top:10, left:10, color:'#fff', zIndex:20003 });
      boxClose
        .css({ height:jQuery(window).height(), position:'fixed', top:0, right:0, cursor:'pointer', background:'url(images/lightclose.png) 95% 20px no-repeat', zIndex:20003 })
        .hover( function(){ jQuery(this).css({ backgroundImage:'url(images/lightclose_.png)' }) }, function(){ jQuery(this).css({ backgroundImage:'url(images/lightclose.png)' }) } )
        .click(function(){ _remove(); });
            
      if( options.controlsShow ){ 
        controls
          .css({ width:boxContainer.width(), height:options.controlsHeight, position:'absolute', bottom:10, textAlign:'right' })
        submit
          .css({ padding:'2px 8px 2px 8px', marginRight:-2, textTransform:'uppercase', fontSize:'9pt', cursor:'pointer', fontWeight:'normal', background:'url(images/green-back.png)', color:'#ffffff', '-webkit-box-shadow': '1px 1px 5px #4e4e4e', '-moz-box-shadow': '1px 1px 5px #4e4e4e', 'box-shadow': '1px 1px 5px #4e4e4e' })
          .hover(
              function(){ jQuery(this).css({ background:'url(images/red-back.png)' }) }, 
              function(){ jQuery(this).css({ background:'url(images/green-back.png)' }) })
          .click(function(){ _submit(); return false; })      
      }else{
        controls.hide()
      }
      
    }
    
    function _build(){       
      var div=document.createElement('div');
          jQuery(div).attr({id:'_lightBox-shadow'});      
          jQuery('body').append(div);
          fadeBack=jQuery('#_lightBox-shadow'); 
          div=null;      
      
      div=document.createElement('div');
          jQuery(div).attr({id:'_box-container'});
          jQuery('body').append(div);
          boxContainer=jQuery('#_box-container'); 
          div=null;
      
      div=document.createElement('div');
          jQuery(div).text('X');
          jQuery(div).attr({id:'_close-x'});
          boxContainer.append(div);
          closeX=boxContainer.find('#_close-x'); 
          div=null;
      
      div=document.createElement('div');
          jQuery(div).attr({id:'_content-box'});
          boxContainer.append(div);
          contentBox=boxContainer.find('#_content-box'); 
          div=null;
      
      div=document.createElement('div');
          jQuery(div).attr({id:'_controls'});
          boxContainer.append(div);
          controls=boxContainer.find('#_controls'); 
          div=null;
      
      div=document.createElement('a');
          jQuery(div).text( options.submitText );
          jQuery(div).attr({id:'_submit'});
          controls.append(div);
          submit=controls.find('#_submit'); 
          div=null;  
           
      div=document.createElement('div');
          jQuery(div).text( options.boxTitle );
          jQuery(div).attr({id:'_box-title'});
          contentBox.before(div);
          boxTitle=boxContainer.find('#_box-title'); 
          div=null;
      
      div=document.createElement('div');
          jQuery(div).attr({id:'_box-error'});
          contentBox.after(div);
          boxError=boxContainer.find('#_box-error'); 
          div=null;
      
      div=document.createElement('div');
          jQuery(div).attr({id:'_box-message'});
          contentBox.after(div);
          boxMessage=boxContainer.find('#_box-message'); 
          div=null;
          
      div=document.createElement('div');
          jQuery(div).attr({id:'_box-description'});
          jQuery('body').append(div);
          boxDescr=jQuery('#_box-description'); 
          div=null;  
          
      div=document.createElement('div');
          jQuery(div).attr({id:'_box-close'});
          jQuery('body').append(div);
          boxClose=jQuery('#_box-close'); 
          div=null;          
    }   
    
  }
})(jQuery)