(function($){
  $.fn.video = function($){
    this.empty().append('<div id="video-name"></div><div id="video-thumbs"></div><div id="video-player"></div><a href="#" id="video-descr">Прочитать описание</a><div id="video-clear"></div><div id="video-overlay"></div><div id="video-descr-text"><div id="video-overlay-close" title="Закрыть">X</div></div>')
    var jqContainer=this, jqName=this.find('#video-name'), jqPlayer=this.find('#video-player'), jqDescr=this.find('#video-descr'), jqThumbs=this.find('#video-thumbs'), jqOverlay=this.find('#video-overlay'), jqDescrText=this.find('#video-descr-text'), jqDescrClose=this.find('#video-overlay-close')
    var videos=[]
    var options={}
    var jscroll=false
    var defaults={
      height:521,
      width:900,
      thumbHeight:50,
      thumbWidth:100,
      diffPlayer:10,        //уменьшение плеера
      autosize:true,
      scrollType:'jscroll', //jscroll rows unlimit
      loader:'ajax-loader.gif'
    }
    
    if(-[1,]){
      var back=new Image()
      back.src='images/'+options.loader
      back.onload =function(){ start() }
      back.onerror=function(){ start() }
    }else{ start(); } 
    
    function start(){
      if( setOptions() ){
        for(var i=0;i<$.length;i++){
        
          videos[i]={}
          videos[i].name=$[i].name
          videos[i].descr=$[i].descr
          videos[i].link=$[i].link
          videos[i].thumb=new Image
          videos[i].thumb.src=$[i].thumb
          videos[i].thumbLoaded=false
          videos[i].videoLoaded=false   
                 
          jQuery(videos[i].thumb).css({ width:options.thumbWidth}).attr('rel',i)
          
          var div=document.createElement('div')
          jQuery(div).addClass('video-thumb').css({ width:options.thumbWidth, background:'url(/images/'+options.loader+') 50% 50% no-repeat' })
          jQuery(div).attr({ title:videos[i].name })
          jQuery(div).append(videos[i].thumb)
          jqThumbs.append(div)
          
          videos[i].thumb.onload=function(){ 
            var i=parseInt( jQuery(this).attr('rel') )
            videos[i].thumbLoaded=true
            jQuery(videos[i].thumb).animate({opacity:1})
            jQuery(videos[i].thumb).parent().css({background:'none'})             
            videos[i].thumb.onload=function(){}
          }  
          jQuery(div).click(function(){ //alert('dfd')
            var id=parseInt( jQuery(this).children().attr('rel') )
            location.hash=id
            showVideo()
          })        
        }        
        switch (options.scrollType ){
          case 'jscroll': { 
            jqThumbs.css({ width: options.thumbWidth+15})
            jqPlayer.css({ width: jqPlayer.width()-10})
            jqThumbs.jScrollPane({autoReinitialise:true}); 
          }break; 
        } 
        //запускаем видео        
        showVideo()                    
      }
    }
    
    function getCurrent(){
      var hash=location.hash
      var id=hash.replace(/^#/,'')        
      id=(id=='')?0:parseInt(id)
      id=( id>=0&&id<=videos.length-1 )?id:0   
      return id
    }
    
    function showVideo(){
      var id=getCurrent()          
      prepareFrame(videos[id].link)
      if(videos[id].descr!='') jqDescr.show() 
      else jqDescr.hide()      
      jqName.text(videos[id].name)
      jqContainer.find('._current').removeClass('_current')
      jqContainer.find('.video-thumb img[rel='+id+']').parent().addClass('_current')
    }
    
    function prepareFrame(frame){
      var regexp=/width="([^"]+)" height="([^"]+)"/
      var result=regexp.exec(frame)
      var width=parseInt(result[1]), height=parseInt(result[2])      
      var new_width, new_height
      new_width=jqPlayer.width()
      new_height=height*new_width/width      
      jqPlayer.css({ height:new_height, background:'url(images/'+options.loader+') 50% 50% no-repeat' })
      jqPlayer.empty().append(frame).children().css({ width:new_width, height:new_height, opacity:0 }).load(function(){
        jqPlayer.children().css({ opacity:1 })  
        jqPlayer.css({ background:'none' })
      })
    }
    
    //открываем описание видео
    jqDescr.click(function(){      
      jqDescrText.css({ top: ( jqOverlay.height()-jqDescrText.height() )/4, left: ( jqOverlay.width()-jqDescrText.width() )/2 })
      var id=getCurrent()
      jqContainer.find('#video-descr-text-text').remove() 
      jqDescrClose.after('<div id="video-descr-text-text"></div>')
      jqContainer.find('#video-descr-text-text').html( videos[id].descr ).jScrollPane({autoReinitialise:true}) 
      jqOverlay.fadeIn()
      jqDescrText.fadeIn()
      return false
    })
    
    jqDescrClose.click(function(){ 
      jqOverlay.fadeOut()
      jqDescrText.fadeOut()
      return false
    })
    
    
    jqOverlay.click(function(){
      jqOverlay.fadeOut()
      jqDescrText.fadeOut()
      return false
    })
        
    function setOptions(){
      if($==null ) return false
      options.autosize=($.autosize!=null)?$.autosize:defaults.autosize
      if(options.autosize){
        options.height=jqContainer.height()
        options.width=jqContainer.width()
      }else{
        options.height=($.height!=null)?$.height:defaults.height
        options.width=($.width!=null)?$.width:defaults.width
      }
      options.thumbHeight=($.thumbHeight!=null)?$.thumbHeight:defaults.thumbHeight
      options.thumbWidth=($.thumbWidth!=null)?$.thumbWidth:defaults.thumbWidth
      options.scrollType=($.scrollType!=null)?$.scrollType:defaults.scrollType
      options.loader=($.loader!=null)?$.loader:defaults.loader
      options.diffPlayer=($.diffPlayer!=null)?$.diffPlayer:defaults.diffPlayer
      
      jqThumbs.css({ width:options.thumbWidth })      
      jqPlayer.css({ width:options.width-options.thumbWidth-options.diffPlayer })      
      
      return true
    }   
    
  }
})(jQuery);