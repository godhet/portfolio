$(document).ready(function(){

  $('#menu a').click(function(){ if( $(this).parent().find('ul').length!=0 ) return false })  

  var headerW=$('#header').width(), wrapperW=$('#wrapper').width(), footerW=$('#footer').width()
  var headerH=$('#header').height(), wrapperH=$('#wrapper').height(), footerH=$('#footer').height()
  var new_height=$(window).height()-footerH-headerH-20
  var new_width=wrapperW*new_height/wrapperH
  
  $('#wrapper').css({ height:new_height, width:new_width })
  $('#header, #footer').css({ width:$('#wrapper').width() })
  $('#content').css({ width:$('#wrapper').width(), height:$('#wrapper').height()-10 })
  $('#content iframe').css({ width:$('#wrapper').width(), height:$('#wrapper').height()-70, background:"url(images/loading-pf.gif) 50% 50% no-repeat" }).load(function(){ $(this).css({ background:'none' }) })
  if( $('#slideshow').length!=0 ) $('#slideshow').css({ width:$('#wrapper').width(), height:$('#wrapper').height() }).rotator( images )

})