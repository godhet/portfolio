/*    
  author Andrew Osipov
  http://godhet.ru
  http://simplephotoweb.ru
*/

function recalc(type){
  var adults=$('select[name=count-aaa]');
  var teens=$('select[name=count-rbg]');
  var childs=$('select[name=count-rmg]');
  var maxCount=5, adultsCount=parseInt(adults.val()), teensCount=parseInt(teens.val()), childsCount=parseInt(childs.val());
    
  if( type=='adults' ){
    var adultsNew=maxCount-adultsCount-teensCount-childsCount;
    var teensNew=maxCount-adultsCount-childsCount
    var childsNew=adultsCount;
    if( childsNew+teensNew+adultsNew>=maxCount ){ //alert('fg')
      childsNew=maxCount-teensCount-adultsCount; 
      childsNew=( childsNew>adultsCount )?adultsCount:childsNew; 
      childsNew=( childsNew+adultsCount>=maxCount )?maxCount-adultsCount-teensCount:childsNew;
      childsNew=( childsCount+adultsCount>=maxCount )?maxCount-adultsCount-childsCount:childsNew;  
    }else if( childsNew+teensNew+adultsCount>=maxCount ){//alert(childsNew)
      childsNew=maxCount-adultsCount-teensNew;
      //childsNew=( childsNew+adultsNew>maxCount )?maxCount-adultsNew:childsNew;      
    }
    
    teens.empty();
    for( var i=0; i<=teensNew; i++){
      teens.append('<option value="'+i+'">'+i+'</option>')
    }
    teens.find('option[value='+teensCount+']').attr('selected','selected');
    childs.empty();
    for( var i=0; i<=childsNew; i++){
      childs.append('<option value="'+i+'">'+i+'</option>')
    }
    childs.find('option[value='+childsCount+']').attr('selected','selected');
    if( adultsCount==0 ){ 
      childs.empty(); childs.append('<option value="0">0</option>'); teens.empty(); teens.append('<option value="0">0</option>'); 
      adults.empty(); for( var i=0; i<=maxCount; i++){ adults.append('<option value="'+i+'">'+i+'</option>'); } 
    }
  }else if( type=='teens' ){
    var adultsNew=maxCount-teensCount-childsCount;
    var teensNew=maxCount-adultsCount-childsCount
    var childsNew=adultsCount;
    if( childsNew+teensNew+adultsNew>=maxCount ){ childsNew=maxCount-teensCount-adultsCount; childsNew=( childsNew>adultsCount )?adultsCount:childsNew; }
  
    adults.empty();
    for( var i=0; i<=adultsNew; i++){
      adults.append('<option value="'+i+'">'+i+'</option>')
    }
    adults.find('option[value='+adultsCount+']').attr('selected','selected');adults.find('option[value='+adultsCount+']').attr('selected','selected');
    childs.empty();
    for( var i=0; i<=childsNew; i++){
      childs.append('<option value="'+i+'">'+i+'</option>')
    } 
    childs.find('option[value='+childsCount+']').attr('selected','selected');childs.find('option[value='+childsCount+']').attr('selected','selected');
  }else if( type=='childs' ){
    var adultsNew=maxCount-teensCount-childsCount;
    var teensNew=maxCount-adultsCount-childsCount
    //var childsNew=adultsCount;
    //if( childsNew+teensNew+adultsNew>=maxCount ){ childsNew=maxCount-teensCount-adultsCount; childsNew=( childsNew>adultsCount )?adultsCount:childsNew; }
  
    adults.empty();
    for( var i=0; i<=adultsNew; i++){
      adults.append('<option value="'+i+'">'+i+'</option>')
    }
    adults.find('option[value='+adultsCount+']').attr('selected','selected');adults.find('option[value='+adultsCount+']').attr('selected','selected');
    teens.empty();
    for( var i=0; i<=teensNew; i++){
      teens.append('<option value="'+i+'">'+i+'</option>')
    }
    teens.find('option[value='+teensCount+']').attr('selected','selected');
    
    
  }
}

$(document).ready(function(){
	
  /*$('#header .login_').click(function(){
	  $('#header .login-form').css('display', 'inline-block');
	  return false;
  })

  $('#header .cancel').click(function(){
	  $('#header .login-form').css('display','none');
	  return false;
  })

  $('#header .login-form form').submit(function(){
	  //alert( $(this).find('input[name="password"]').val() )
	  $.ajax(
		"http://booking.scat.kz/oxygen/json/login",
		{ 
			type:'POST',
			crossDomain:true,
			data:{ login:$(this).find('input[name="login"]').val(), password:$(this).find('input[name="password"]').val() },
			complete: function(jqXHR, textStatus){
				var e;
				//alert(textStatus)
			}
		}
	  ); 
	 
	  return false; 
  }) */
  
  var ___timer=setInterval(function(){
	 if( $('head link[rel="icon"]') ){ 
		$('head link[rel="icon"]').remove();
		$('head').append('<link href="http://www.scat.kz/images/favicon.png" rel="shortcut icon" type="image/x-icon" />');
		clearInterval(___timer)
	 }
  },100)

  recalc('adults');
  $('select[name=count-aaa]').change(function(){
     recalc('adults');
  })
  
  $('select[name=count-rbm]').change(function(){
     recalc('teens');
  })
  
  $('select[name=count-rmg]').change(function(){
     recalc('childs');
  })
  
  //--------------------

  $('#ind-blocks li:first div').each(function(i,e){
    var text=$(e).text();
    if( text.length>147 ) $(e).html( text.substr(0,150)+'...' )
    else $(e).html(text);
  })  
  
  $('#sub-menu-thumbs li a span').each(function(i,e){
    var text=$(e).text();
    //if( $(this).parent().attr('href')=='advertising-on-the-headrests.html' ){ $(this).css({ paddingRight:2 }) }
  })
  
  $('#sub-menu-thumbs li').each(function(i,e){
    var text=$(e).text();
    //if( $(this).parent().attr('href')=='advertising-on-the-headrests.html' ){ $(this).css({ paddingRight:2 }) }
    if( i==1 || i==4 || i==7 || i==10 || i==13 || i==16 ){ //alert(i)
      $(e).css({ marginRight:35, marginLeft:35 })
    }    
  })
  
  $('#news-thumbs li').each(function(i,e){
    if( i==1 || i==4 || i==7 || i==10 || i==13 || i==16 ){ 
      $(e).css({ marginRight:35, marginLeft:35 })
    }    
  })
  
  //for( var i=1;i<$('#sub-menu-thumbs li').length-1; 3*i-1 ){
   //  $('#sub-menu-thumbs li').eq(i).css({ marginRihgt:35, marginLeft:35 })
  //}
  
  
  if( $('.slider').length==2 ){
    $('.slider:first')._slider({ stock: true });
    $('.slider:last')._slider({ stock: !true });
  }
  
  $('#booking-trail li:last').css({ background:'none' })
  $('#booking-form #booking-form-left .field:last, #booking-form #booking-form-right .field:last').css({ border:'none' })
  $('#booking-counts li ul li').click(function(){ 
    $(this).parent().find('.active').removeClass('active');
    $(this).addClass('active');
  })

  $('#ticket-cont .radio > div').click(function(event){
    event.stopPropagation();
    $(this).parent().find('.checked').removeClass('checked');
    $(this).find('input[type=radio]').attr({checked:'1'})
    $(this).find('span').addClass('checked');
    $(this).addClass('checked');    
  })
  
  $('#ticket-cont  .radio .dir2').click(function(){ 
    var rel=$(this).parent().parent().find('.back-date').val();
    $(this).parent().attr( 'rel', rel );
    $(this).parent().parent().find('.back-date').hide();
    $(this).parent().parent().find('.back-date').val( '' );    
    //alert('hjh')
    return false;
  })
  
  $('#ticket-cont .radio .dir1').click(function(){
    //alert( $(this).length )
    if( $(this).parent().attr('rel') != null ){
      $(this).parent().parent().find('.back-date').show(); 
      var rel=$(this).parent().attr('rel');
      $(this).parent().parent().find('.back-date').val( rel );
      //$(this).parent().attr('rel','');
    }
    return false;
  })
  
  $('#sub-menu-thumbs li, #news-thumbs li').each(function(i,e){ 
     //if( (i+1)%3==0 ){  }
  });
  
  $('.faq-name').click(function(){
    $('.faq-name').next().slideUp();
    $(this).next().slideDown()
  })
  
  /*$('#ticket-cont li a').click(function(){
    $(this).parent().parent().find('.active').removeClass('active');
    $(this).parent().addClass('active');
    ///alert( $(this).parent().attr('id') )
    if( $(this).parent().attr('id')=='ticket-cont-id1' ){ 
      $('#ticket-cont .field.passenger').show();
      $('#ticket-form input[name=page]').val('avia.flights-tariffication')
    }
    if( $(this).parent().attr('id')=='ticket-cont-id2' ){
      $('#ticket-cont .field.passenger').hide();
      $('#ticket-form input[name=page]').val('avia.b2c.period-tariffs')
    }
    if( $(this).parent().attr('id')=='ticket-cont-id3' ){
      $('#ticket-cont .field.passenger, #ticket-cont .field.passenger').hide();
      $('#ticket-form input[name=page]').val('avia-info.ordersearch')
    }
    return false;
  })*/
  
  $('#ticket-cont li a.nav').click(function(){
    if( $(this).parent().attr('id')=='ticket-cont-id2' ){ return !false; }
    //if( $(this).parent().find('ul').length>0 ){ return false; }
    $(this).parent().parent().find('.active').removeClass('active');
    $(this).parent().addClass('active');
    $(this).parent().find('a+div').show();
    return false;
  })
  
  $('#ticket-cont li ul a').click(function(){
    return true;
  })
  
  $('#flex_search').click(function(){    
    if( $(this).attr('checked') ){
      $(this).val('1');
	  $('#ticket-form input[name=page]').val('avia.b2c.period-tariffs');
    }else{
      $(this).val('0');
	  $('#ticket-form input[name=page]').val('avia.flights-tariffication');      
    }
  }) 
  
  var months, days; 
  if( lang=='ru' ){
    months=["Январь","Февраль","Март","Апрель","Май","Июнь","Июль","Август","Сентябрь","Октябрь","Ноябрь","Декабрь"];
    days=["Воскресенье","Понедельник","Вторник","Среда","Четверг","Пятница","Суббота"];
  }
  if( lang=='en' ){
    months=["January","February","March","April","May","June","July","August","September","October","November","December"];
    days=["Воскресенье","Понедельник","Вторник","Среда","Четверг","Пятница","Суббота"];
  }
  if( lang=='kz' ){
    months=["Қаңтар","Ақпан","Наурыз","Сәуір","Май","Маусым","Шілде","Тамыз","Қыркүйек","Қазан","Қараша","Желтоқсан"];
    days=["Воскресенье","Понедельник","Вторник","Среда","Четверг","Пятница","Суббота"];
  }
  if( $("input[name^=date]").length>0 ){
    $("input[name^=date]").datepicker({
		  dayNames:days,
      dayNamesShort:["Вск","Пнд","Втр","Срд","Чтв","Птн","Сбб"],
      dayNamesMin:["Вс","Пн","Вт","Ср","Чт","Пт","Сб"],
      closeText:"Готово",
      prevText:"Назад",
      nextText:"Вперед",
      currentText:"Today",
      monthNames:months,
      monthNamesShort:["Янв","Фев","Мар","Апр","Май","Июн","Июл","Авг","Сен","Окт","Ноя","Дек"],
      dateFormat: "dd.mm.yy",
      firstDay: 1,
      numberOfMonths:2	
	 });
    $("input[name=back-date]").datepicker({
			dayNames:["Воскресенье","Понедельник","Вторник","Среда","Четверг","Пятница","Суббота"],
      dayNamesShort:["Вск","Пнд","Втр","Срд","Чтв","Птн","Сбб"],
      dayNamesMin:["Вс","Пн","Вт","Ср","Чт","Пт","Сб"],
      closeText:"Готово",
      prevText:"Назад",
      nextText:"Вперед",
      currentText:"Today",
      monthNames:months,
      monthNamesShort:["Янв","Фев","Мар","Апр","Май","Июн","Июл","Авг","Сен","Окт","Ноя","Дек"],
      dateFormat: "dd.mm.yy",
      firstDay: 1,
      numberOfMonths:2
	 });
  } 
	if( $('#ticket-form form').length>0 ){		
		$('#ticket-form form').submit(function(){
			if( $(this).find('.dir1.checked').length>0 ){
				var origin=$(this).find('.origin-city').val();
				var destination=$(this).find('.destination-city').val();
				$(this).append("<input type='hidden' name='origin-city-code[1]' value='"+destination+"' />");
				$(this).append("<input type='hidden' name='destination-city-code[1]' value='"+origin+"' />");
				$(this).find("input[name='segmentsCount']").val('2')
			}else{
				$(this).find("input[name='segmentsCount']").val('1');
			}
			//if()
			return true;			
		})
	}
})